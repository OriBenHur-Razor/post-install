#!/usr/bin/env bash

if [[ "$EUID" -ne 0 ]]; then
  echo "Please run as root Or run 'sudo bash $0'"
  exit 0
fi

[[ -f /etc/apt/sources.list.d/teamviewer.list ]] && sudo rm /etc/apt/sources.list.d/teamviewer.list
sudo add-apt-repository ppa:deadsnakes/ppa -y
sudo apt-get update
sudo apt-get install --no-install-recommends -y software-properties-common \
 curl \
 gpg-agent \
 gnupg \
 lsb-release \
 gcc \
 make \
 build-essential \
 ca-certificates \
 apt-transport-https \
 linux-headers-"$(uname -r)"

sudo apt install -y python3.7 \
 python3-pip \
 python3-opencv \
 libffi-dev \
 libssl-dev \
 python3-dev

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
sudo apt-key adv --fetch-keys http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64/7fa2af80.pub
curl -fsSL https://download.teamviewer.com/download/linux/signature/TeamViewer2017.asc | sudo apt-key add -
curl -fsSL https://dl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
echo "deb http://linux.teamviewer.com/deb stable main" | sudo tee /etc/apt/sources.list.d/teamviewer.list
echo "deb http://linux.teamviewer.com/deb preview main" | sudo tee -a /etc/apt/sources.list.d/teamviewer.list
echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" | sudo tee /etc/apt/sources.list.d/google-chrome.list
echo "deb http://developer.download.nvidia.com/compute/cuda/repos/ubuntu1804/x86_64 /" | sudo tee /etc/apt/sources.list.d/cuda.list
echo "deb http://developer.download.nvidia.com/compute/machine-learning/repos/ubuntu1804/x86_64 /" | sudo tee /etc/apt/sources.list.d/cuda_learn.list
sudo tee /etc/apt/sources.list.d/docker.list <<< "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
sudo add-apt-repository ppa:graphics-drivers/ppa -y
sudo add-apt-repository universe -y
distribution=$(
  . /etc/os-release
  echo "$ID""$VERSION_ID"
)
curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add -
curl -s -L https://nvidia.github.io/nvidia-docker/"${distribution}"/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
sudo apt update
sudo apt install -y nvidia-driver-460 cuda-toolkit-10-2 libcudnn7 libcudnn7-dev

sudo apt install --no-install-recommends --yes --force-yes -o Dpkg::Options::="--force-confnew" docker-ce \
 docker-ce-cli \
 containerd.io \
 ncdu \
 unzip \
 gzip \
 iotop \
 iftop \
 lsof \
 mtools \
 nmap \
 v4l-utils \
 vim \
 ntfs-3g \
 hwdata \
 hardinfo \
 exfat-fuse \
 exfat-utils \
 htop \
 ssh \
 tmux \
 gparted \
 git \
 sysstat \
 inotify-tools \
 ffmpeg \
 teamviewer \
 terminator \
 jq \
 pv
snap install pycharm-community --classic
tag="$(curl -s https://api.github.com/repos/docker/compose/releases/latest | jq -r '.tag_name')"
sudo curl -L "https://github.com/docker/compose/releases/download/${tag}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose && sudo chmod +x /usr/local/bin/docker-compose
sudo curl -L "https://raw.githubusercontent.com/docker/compose/${tag}/contrib/completion/bash/docker-compose" -o /etc/bash_completion.d/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
sudo groupadd docker
sudo usermod -aG docker "$(logname)"
chown -R "$(logname)":"$(logname)" ~
